/*********************************************************************
* Filename:   dh.c
* Author:     weijiel6
*********************************************************************/
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <strings.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>


#define ALPHA_WEIGHT	87	/* alpha value at ascII */
#define INT_WEIGHT		48	/* integer value at ascII */
#define G_VALUE			15	/* g value */
#define P_VALUE			97	/* p value */
#define SENDB_SIZE		3	/* initialise sendB array size */
#define SENDKEY_SIZE	10	/* initialise sendkeyb array size */

/****************************************************************/
int change_dec(char a);
int compute(int g, int m, int p);
/****************************************************************/





// use to calclate the (g)b mod p or (g)ba mod p
int compute(int g, int m, int p) {
	
	// initialise root and result for return
	int r;
	int result = 1;
	
	//while (g)b or (g)ba greater than 0
	while (m > 0) {
		r = m % 2;
	
		// find the root and calculate the result
		if (r == 1){
			result = (result*g) % p;
		}
		g = g*g % p;
	
		m = m / 2;
	}
	return result;
}

// change the char to integer
int change_dec(char a) {
	
	//initialise a result for return
	int result;
	if(isalpha(a)) {
		//fix with alpha case
		result = a - ALPHA_WEIGHT;
	} else {
		//fix with integer case
		result = a - INT_WEIGHT;
	}
	
	return result;
}

int main( int argc, char **argv ) {
	
	//set the g and p value
	int g = G_VALUE;
	int p = P_VALUE;
	
	//set the command and the order for the system
	char command[50];
	strcpy(command, "openssl sha256 dh.c >v.txt" );
	system(command);
	
	//create a v.txt for store the hash value
	FILE *fp;
	fp = fopen("v.txt", "r");
	while((fgetc(fp) != EOF) && (fgetc(fp) != ' ')) {
		
	}
	
	//get the first and second argument for the v.txt
	char first_arg = fgetc(fp);
	char second_arg = fgetc(fp);
	
	//create result a and b for store change demecial from first and second argument
	int result_a;
	int result_b;
	
	result_a = change_dec(first_arg);
	result_b = change_dec(second_arg);
	
	int b = (result_a * 16) + result_b;
	
	//calculate B value
	int B = compute(g, b, p);
	
	
	//store into sendB array
	char send_B[SENDB_SIZE];
	sprintf(send_B, "%d\n", B); 

	
	
	int sockfd, portno, n;
    struct sockaddr_in serv_addr;
    struct hostent * server;

    char buffer[256];

    portno = 7800;


    /* Translate host name into peer's IP address ;
     * This is name translation service by the operating system*/
     
    server = gethostbyname("172.26.37.44");
    /* Building data structures for socket */

    bzero((char *)&serv_addr, sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;

    bcopy(server->h_addr_list[0], (char *)&serv_addr.sin_addr.s_addr, server->h_length);

    serv_addr.sin_port = htons(portno);

    /* Create TCP socket -- active open
    * Preliminary steps: Setup: creation of active open socket*/
    

    sockfd = socket(AF_INET, SOCK_STREAM, 0);

    if (sockfd < 0)
    {
        perror("ERROR opening socket");
        exit(0);
    }

    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0)
    {
        perror("ERROR connecting");
        exit(0);
    }
	
	
	//send weijiel6 message
	bzero(buffer, 256);

    strcpy(buffer, "weijiel6\n");

    n = write(sockfd, buffer, strlen(buffer));
	
	//send sendB message
	bzero(buffer, 256);
	
	strcpy(buffer, send_B);
	
	n = write(sockfd, buffer, strlen(buffer));
	
	
    if (n < 0)
    {
        perror("ERROR writing to socket");
        exit(0);
    }
	
	//receive A  value
    bzero(buffer, 256);

    n = read(sockfd, buffer, 255);

    if (n < 0)
    {
        perror("ERROR reading from socket");
        exit(0);
    }
	
	//change A into integer
	int A = atoi(buffer);
	
	//calculte key value
	int keyB = compute(A, b, p);
	
	//store key into send keyB
	char send_keyB[SENDKEY_SIZE];
	sprintf(send_keyB, "%d\n", keyB); 
	
	//send send keyB message
	bzero(buffer, 256);
	
	strcpy(buffer, send_keyB);
	
	n = write(sockfd, buffer, strlen(buffer));
	
	//get the response
	bzero(buffer, 256);

    n = read(sockfd, buffer, 255);
	
    printf("%s\n", buffer);

	
   	return 0;
}

