/*********************************************************************
* Filename:   crack.c
* Author:     weijiel6
*********************************************************************/



#include "sha256.h"
#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <limits.h>
#include <stdlib.h>
#include <time.h>
#include <ctype.h>
#include <string.h>
#include <sys/types.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <sys/stat.h>



#define PWD4_SIZE	320	/* pwd4sha256 size */
#define PWD6_SIZE	640	/* pwd6sha256 size */
#define ASCII_ST	32	/* ascII start value */
#define ASCII_ED	127	/* ascII end value */
#define INT_ST		48	/* ascII integer start value */
#define INT_ED		58	/* ascII integer end value */
#define ALPHA_ST	97	/* ascII little alpah start value */
#define ALPHA_ED	124	/* ascII little alpah end value */

/****************************************************************/
int check_guess(BYTE secrets[], BYTE hash[], int size);
int get_password(char *line, FILE *fp);
/****************************************************************/


//get the password
int get_password(char *line, FILE *fp) {
	int i = 0, c;
	
	//if no eof or n or f, get the char
	while(((c = fgetc(fp)) != EOF) && (c !='\n') && (c != '\r')) {
		line[i++] = c;
	}
	
	//add \0 at final place
	line[i] = '\0';
	return 1;
}



// check the guess result and return the flag
int check_guess(BYTE secrets[], BYTE hash[], int size) {
	
	//initialise the compare size and flag
	int compare_size = 32;
	int flag;
	
	//find how many turns should run
	int turn = (size /32) + 1;
	
	//for each turn compare the hash value between data and secret
	for(int i = 1 ; i < turn ; i++) {
		flag = -1;
		
		
		for(int k = (i - 1) *compare_size; k < i *compare_size; k++) {
			
			//if not mach return 0
			if (secrets[k] != hash[k-((i - 1) *compare_size)]) {
				flag = 0;
				break;
			}else{
				flag=1;
			}
			
		}
		
		// if match, return the place of the secret
		if(flag == 1){

			return flag * i;
		}
	}

	return flag;
}


int main( int argc, char **argv )
{
	
	/* check we have 0 argument, test for guessing password*/
	if( argc < 2 )
	{	
		//open fp and fp1 for pwd4sha256 and write found_pwd
		FILE *fp;
		FILE *fp1;
		int pwd4_size;
		fp1 = fopen("found_pwds.txt", "w");
		BYTE secrets[PWD4_SIZE];
		fp = fopen("pwd4sha256", "rb");
		fread(secrets, 320, 1, fp);
		/*Move file point at the end of file.*/
    	fseek(fp, 0, SEEK_END);
     
    	/*Get the current position of the file pointer.*/
    	pwd4_size=ftell(fp);
		
		fseek(fp, 0, SEEK_SET); 
		
		fclose(fp);
		
		//initialise guess needed array and text
		char guess[5];
		int secret_num;
		SHA256_CTX ctx;
		BYTE data[SHA256_BLOCK_SIZE];
		
		//find for whole ascII
		for(int i = ASCII_ST; i < ASCII_ED; i++) {
			for(int j = ASCII_ST; j < ASCII_ED; j++) {
				for(int l = ASCII_ST; l < ASCII_ED; l++) {
					for(int k = ASCII_ST; k < ASCII_ED; k++) {
						
						//store into guess
						sprintf(guess, "%c%c%c%c", i, j, l, k);

						//hash the guess				
						sha256_init(&ctx);
						sha256_update(&ctx, (BYTE*)guess, strlen(guess));
						sha256_final(&ctx, data);
						
						//chech the hash 
						if (check_guess(secrets, data, pwd4_size) > 0) {
							secret_num = check_guess(secrets, data, pwd4_size);
							printf("%s %d\n",guess, secret_num);
							fprintf(fp1, "%s %d\n", guess, secret_num);							
						}
						

					}
				}
			}
		}
		
		guess[5] = '\0';
		
		
		//open the pwd6sha256
		FILE *fp3;
		BYTE long_secrets[PWD6_SIZE];
		fp3 = fopen("pwd6sha256", "rb");
		fread(long_secrets, 640, 1, fp3);
		int pwd6_size;
		/*Move file point at the end of file.*/
    	fseek(fp3, 0, SEEK_END);
     
    	/*Get the current position of the file pointer.*/
    	pwd6_size=ftell(fp3);
    	
    	fseek(fp3, 0, SEEK_SET);
    	

		//create longer gusee for 6 password guess
		char long_guess[7];
		int long_secret_num;
		SHA256_CTX long_ctx;
		BYTE long_data[SHA256_BLOCK_SIZE];
		
		//check all number guess
		for(int i = INT_ST; i < INT_ED; i++) {
			for(int j = INT_ST; j < INT_ED; j++) {
				for(int l = INT_ST; l < INT_ED; l++) {
					for(int k = INT_ST; k < INT_ED; k++) {
						for(int n = INT_ST; n < INT_ED; n++) {
							for(int m = INT_ST; m < INT_ED; m++) {
								
								//store into long guess
								sprintf(long_guess, "%c%c%c%c%c%c", i, j, l, k, n, m);

								//hash the long guess				
								sha256_init(&long_ctx);
								sha256_update(&long_ctx, (BYTE*)long_guess, strlen(long_guess));
								sha256_final(&long_ctx, long_data);
								
								//chech the hash
								if (check_guess(long_secrets, long_data, pwd6_size) > 0) {
									long_secret_num = check_guess(long_secrets, long_data, pwd6_size);
									
									//add 10 for 10 later password
									long_secret_num = long_secret_num + 10;
									printf("%s %d\n",long_guess, long_secret_num);
									fprintf(fp1, "%s %d\n", long_guess, long_secret_num);							
								}
							}
						}
					}	
				}
			}
		}
		long_guess[7] = '\0';
		
		//create alpha gusee for 6 password guess
		char alpha_guess[7];
		int alpha_secret_num;
		SHA256_CTX alpha_ctx;
		BYTE alpha_data[SHA256_BLOCK_SIZE];
		
		//check all little alpha guess
		for(int i = ALPHA_ST; i < ALPHA_ED; i++) {
			for(int j = ALPHA_ST; j < ALPHA_ED; j++) {
				for(int l = ALPHA_ST; l < ALPHA_ED; l++) {
					for(int k = ALPHA_ST; k < ALPHA_ED; k++) {
						for(int n = ALPHA_ST; n < ALPHA_ED; n++) {
							for(int m = ALPHA_ST; m < ALPHA_ED; m++) {								
								//store into little alpha guess
								sprintf(alpha_guess, "%c%c%c%c%c%c", i, j, l, k, n, m);
								
								//hash the little alpha guess			
								sha256_init(&alpha_ctx);
								sha256_update(&alpha_ctx, (BYTE*)alpha_guess, strlen(alpha_guess));
								sha256_final(&alpha_ctx, alpha_data);
								
								//chech the hash
								if (check_guess(long_secrets, alpha_data, pwd6_size) > 0) {
									alpha_secret_num = check_guess(long_secrets, alpha_data, pwd6_size);

									//add 10 for 10 later password
									alpha_secret_num = alpha_secret_num + 10;
									printf("%s %d\n",alpha_guess, alpha_secret_num);
									fprintf(fp1, "%s %d\n", alpha_guess, alpha_secret_num);							
								}
							}
						}
					}	
				}
			}
		}
		alpha_guess[7] = '\0';
		
		//close all file
		fclose(fp1);
		fclose(fp3);
		
		return 0;
	}

	if( argc == 2 )
	{
		// get the guess number and open diriction
    	int guess_num = atoi(argv[1]);
    	FILE *fp;
		char line[10000];
		char guess_word[100000][7];
		fp = fopen("common_passwords.txt", "r");
		int fp_size;
		fseek(fp, 0, SEEK_END);
    	fp_size=ftell(fp);
    	fseek(fp, 0, SEEK_SET);
    	
    	//set all password into direction
		for(int i = 0; i < fp_size; i++) {
			get_password(line, fp);
			strncpy(guess_word[i], line, 6);
		}
		
		//print out guess based on require number
		for(int i = 0; i < guess_num; i++) {
			if(strlen(guess_word[i]) < 6 && strlen(guess_word[i]) > 0) {
				for(int k = (strlen(guess_word[i]) - 1); k < 6; k++){
					guess_word[i][k] = 'a';
				}
				guess_word[i][6] = '\n'; 
			}
			if(strlen(guess_word[i]) == 0) {
				for(int k = 0; k < 6; k++){
					guess_word[i][k] = 'a';
				}
			}
			 
			printf("%s\n", guess_word[i]);
		}
		
		return 0;
	}
	
	if( argc == 3 )
	{	
		//open argument 1 and 2 file
		FILE *fp1;
		fp1 = fopen(argv[2], "rb");
		int fp1_size;
		fseek(fp1, 0, SEEK_END);
    	fp1_size=ftell(fp1);
    	fseek(fp1, 0, SEEK_SET);
		BYTE secrets[fp1_size];
		fread(secrets, fp1_size, 1, fp1);
		FILE *fp;
		char line[10000];
		fp = fopen(argv[1], "r");
		int secret_num;
		
		//check the password from first argument to the second
		for(int i = 0; i < 10000 ; i++) {
			get_password(line, fp);
			SHA256_CTX ctx;
			BYTE data[SHA256_BLOCK_SIZE];
			sha256_init(&ctx);
			sha256_update(&ctx, (BYTE*)line, strlen(line));
			sha256_final(&ctx, data);
			if (check_guess(secrets, data, fp1_size) > 0) {
				secret_num = check_guess(secrets, data, fp1_size);
				printf("%s %d\n", line, secret_num);							
			}
		}
		
		//close all files
		fclose(fp);
		fclose(fp1);
		return 0;
	}
	
	

}

